/**
 * Program to run on Arduino Uno or Pro Mini. Timer make a device turned on for a configured number of minutes (and or seconds), then turn it off.
 *
 * @author: Juanid PV (junu.pv@gmail.com)
 */

#include <EEPROM.h>

#include <PinChangeInterrupt.h>
#include <PinChangeInterruptBoards.h>
#include <PinChangeInterruptPins.h>
#include <PinChangeInterruptSettings.h>

#include <Wire.h>

#include <LiquidCrystal_I2C.h>


// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// We store number of minutes and seconds in separate locations in EEPROM.
// So we can have timer of maximum 256 minutes + 59 seconds = 4 hours 1 minute 15 seconds.
// But we will be using millis() function so cannot have that much time configured.
const int MINUTES_STORE = 0;
const int SECONDS_STORE = 1;

/**
 * Write timer time value to EEPROM.
 * CAUTION: Avoid calling this function unnecesserily, EEPROM has limitation of maximum write cycles (For Atmega328p it is 100,000).
 */
void timer_time_write(int total_milli_seconds) {
  // Find minutes value to be stored.
  byte minutes_to_store = (total_milli_seconds / 60000) % 60;
  // Find seonds value to be stored.
  byte seconds_to_store = (total_milli_seconds / 1000) % 60;

  // Save values to EEPROM.
  EEPROM.write(MINUTES_STORE, minutes_to_store);
  EEPROM.write(SECONDS_STORE, seconds_to_store);
}

/**
 * Read timer time value from EEPROM.
 *
 * @return Total number of milli seconds to keep the timer.
 */
int timer_time_read() {
  return ((unsigned long) EEPROM.read(MINUTES_STORE)) * 60000ul + ((unsigned long) EEPROM.read(SECONDS_STORE)) * 1000ul;
}

// Define button pressing so it can be easily changed later.
#define BUTTON_PRESSING RISING

const byte POWER_SOURCE = 10;

// It should be high when timer is active.
// Connect to relay.
const byte PUMP_SWITCH = 11;

// Button to start timer running.
const byte START_BUTTON = 4;
// Button to stop timer forcefully.
const byte STOP_CANCEL_BUTTON = 5;
// Button enter in configuration mode.
const byte CONFIG_START_BUTTON = 6;
// Button to increment time being configured.
const byte CONFIG_INCREMENT_BUTTON = 7;
// Button to decrement time being configured.
const byte CONFIG_DECREMENT_BUTTON = 8;
// Button to save time being configured.
const byte CONFIG_SAVE_BUTTON = 9;

// Device will be any of following status.
const byte TIMER_STATUS_IDLE = 0;
const byte TIMER_STATUS_RUNNING = 2;
const byte TIMER_STATUS_CONFIGURING = 4;

// To indicate current status of timer.
volatile byte timer_status;

// Store timestamp when timer started.
volatile unsigned long timer_start_time;
// Store time being configured.
volatile unsigned long timer_time_being_configured;
// Store idele start time.
volatile unsigned long idle_start_time;
// Store time which how long timer to run.
volatile unsigned long timer_time;

const unsigned long idle_time_limit = 20000ul;

unsigned long lcd_updated_time;

/**
 * Activate timer.
 */
void start_timer() {
  // Debounce by checking the flag.
  if (timer_status == TIMER_STATUS_IDLE) {
    timer_status = TIMER_STATUS_RUNNING;
    // Time period in milliseconds.
    timer_time = timer_time_read();

    timer_start_time = millis();
  }
}

/**
 * Stop timer.
 */
void stop_or_cancel() {
  if (timer_status == TIMER_STATUS_RUNNING) {
    stop_timer();
    power_off();
  }
  else if (timer_status == TIMER_STATUS_CONFIGURING) {
    timer_status = TIMER_STATUS_IDLE;
    idle_start_time = millis();
  }
}

void configure_start() {
  if (timer_status == TIMER_STATUS_IDLE) {
    timer_status = TIMER_STATUS_CONFIGURING;
    timer_time_being_configured = timer_time_read();
  }
}

void configure_increment() {
  if (timer_status == TIMER_STATUS_CONFIGURING) {
    // Increment 10 seconds.
    timer_time_being_configured = timer_time_being_configured + 10000ul;
  }
}

void configure_decrement() {
  if (timer_status == TIMER_STATUS_CONFIGURING) {
    // Decrement 10 seconds.
    if (timer_time_being_configured >= 10000ul) {
      timer_time_being_configured = timer_time_being_configured - 10000ul;
    }
  }
}

void configure_set() {
  if (timer_status == TIMER_STATUS_CONFIGURING) {
    timer_time_write(timer_time_being_configured);
    timer_status = TIMER_STATUS_IDLE;
    idle_start_time = millis();
  }
}

void power_off() {
  digitalWrite(POWER_SOURCE, LOW);
}

void stop_timer() {
  digitalWrite(PUMP_SWITCH, LOW);
  timer_start_time = 0;
  idle_start_time = millis();
  timer_status = TIMER_STATUS_IDLE;
}

void show_time(String status, unsigned long time_to_show) {
  lcd.setCursor(0, 0);
  lcd.print(status);
  lcd.setCursor(0, 1);

  int seconds = (time_to_show / 1000ul) % 60;
  int minutes = (time_to_show / 60000ul) % 60;
  int hours = (time_to_show / 3600000ul) % 24;

  lcd.setCursor(0, 1);
  char message[16];
  sprintf(message, "%02d:%02d:%02d", hours, minutes, seconds);
  lcd.print(message);
  lcd_updated_time = millis();
}

void setup() {
  // Start self powering.
  pinMode(POWER_SOURCE,OUTPUT);
  digitalWrite(POWER_SOURCE,HIGH);

  // initialize the LCD
	lcd.begin();

  // Configure output pins.
  pinMode(PUMP_SWITCH, OUTPUT);
  // Configure pins for input push buttons.
  pinMode(START_BUTTON, INPUT_PULLUP);
  pinMode(STOP_CANCEL_BUTTON, INPUT_PULLUP);
  pinMode(CONFIG_START_BUTTON, INPUT_PULLUP);
  pinMode(CONFIG_INCREMENT_BUTTON, INPUT_PULLUP);
  pinMode(CONFIG_DECREMENT_BUTTON, INPUT_PULLUP);
  pinMode(CONFIG_SAVE_BUTTON, INPUT_PULLUP);

  // Ensure timer is not running. We just started the device!
  stop_timer();

  attachPCINT(digitalPinToPCINT(START_BUTTON), start_timer, BUTTON_PRESSING);
  attachPCINT(digitalPinToPCINT(STOP_CANCEL_BUTTON), stop_or_cancel, BUTTON_PRESSING);
  attachPCINT(digitalPinToPCINT(CONFIG_START_BUTTON), configure_start, BUTTON_PRESSING);
  attachPCINT(digitalPinToPCINT(CONFIG_INCREMENT_BUTTON), configure_increment, BUTTON_PRESSING);
  attachPCINT(digitalPinToPCINT(CONFIG_DECREMENT_BUTTON), configure_decrement, BUTTON_PRESSING);
  attachPCINT(digitalPinToPCINT(CONFIG_SAVE_BUTTON), configure_set, BUTTON_PRESSING);

  // lcd.backlight();
	lcd.print("Starting...");
  lcd_updated_time = millis();
}

void loop() {
  unsigned long current_time = millis();
  if (timer_status == TIMER_STATUS_RUNNING) {
    digitalWrite(PUMP_SWITCH, HIGH);
    // millis() will overflow, however it this comparison will always work unless
    // we want to make timer active for more maximum value of millis(). It is approximately 50 days in Atmega328p, Atmega168p, etc.
    if ( (current_time - timer_start_time) >= timer_time ) {
      // Timer elapsed the required time period.
      // So, stop the timer and turn off
      // We turn off entire device instead just stopping timer to save energy
      // device will be most used as timer.
      power_off();
    }
  }
  else if (timer_status == TIMER_STATUS_CONFIGURING) {
    // Nothing to do?
  }
  else if ( (current_time - idle_start_time) >=  idle_time_limit) {
    // Power off if it is already 1 minute since idle started.
    power_off();
  }

  // Update LCD every seconds.
  if ( (current_time - lcd_updated_time) > 1000) {
    if (timer_status == TIMER_STATUS_RUNNING) {
      show_time("Running:", current_time - timer_start_time);
    }
    else if (timer_status == TIMER_STATUS_CONFIGURING) {
      show_time("Setting timer:", timer_time_being_configured);
    }
    else {
      show_time("Turn off by:", idle_time_limit - (current_time - idle_start_time) );
    }
  }
}
