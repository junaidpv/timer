
#include <EEPROM.h>

const int MINUTES_STORE = 0;
const int SECONDS_STORE = 1;

/**
 * Write timer time value to EEPROM.
 * CAUTION: Avoid calling this function unnecesserily, EEPROM has limitation of maximum write cycles (For Atmega328p it is 100,000).
 */
void timer_time_write(int total_milli_seconds) {
  // Find minutes value to be stored.
  int minutes_to_store = (total_milli_seconds / 60000) % 60;
  // Find seonds value to be stored.
  int seconds_to_store = (total_milli_seconds / 1000) % 60;

  // Save values to EEPROM.
  EEPROM.write(MINUTES_STORE, minutes_to_store);
  EEPROM.write(SECONDS_STORE, seconds_to_store);
}

/**
 * Read timer time value from EEPROM.
 *
 * @return Total number of milli seconds to keep the timer.
 */
int timer_time_read() {
  return ((int) EEPROM.read(MINUTES_STORE)) * 60000 + ((int) EEPROM.read(SECONDS_STORE)) * 1000;
}

void setup() {
  // Write 20 secondsto EEPROM.
  timer_time_write(20000ul);

}

void loop() {
  // put your main code here, to run repeatedly:

}
